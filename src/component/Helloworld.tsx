import styles from "./style.css";

export interface IHelloWorld {
  text: string
}
export const Helloworld = ({ text }: IHelloWorld) => {
  return <>
    <style dangerouslySetInnerHTML={{ __html: `${styles}`}}></style>

    <div className="text">This is hello text, yes it is: {text}</div>
  </>
}
