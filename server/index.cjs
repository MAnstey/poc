const express = require('express')
const app = express()
const bodyParser = require('body-parser');
const { createElement } = require('react');
const { renderToString } = require("react-dom/server");
const { Helloworld } = require('../dist/react-vite-library.cjs')


app.use(bodyParser.json())

// respond with "hello world" when a GET request is made to the homepage
app.post('/', (req, res) => {

  console.log(req.body);

  res.send({ html: renderToString(createElement(Helloworld, { text: req.body.text})), css: ".text{font-size:20px;color:red}" })
})


app.listen("40000");
